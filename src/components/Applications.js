import React from 'react'
import { FiEdit, FiDelete } from 'react-icons/fi'

class Applications extends React.Component {
  render() {
    return (
      <div className='applications' >
        {this.props.applications.map((application, index) => {
          return (
            <div className='application' key={index} >
              <div className='button-bar'>
                <button onClick={() => { this.props.editApplication(index) }}> <FiEdit /> </button>
                <button onClick={() => { this.props.deleteApplication(index) }}> <FiDelete /> </button>
              </div>
              <div className='label'> Name:
                              <span> {application.name} </span>
              </div>
              <div className='label'> Age:
                              <span> {application.age} </span>
              </div>
              <div className='label'> Phone:
                              <span> {application.phone} </span>
              </div>
              <div className='label'> Email:
                              <span> {application.email} </span>
              </div>
              <div className='label'> Preferred Way of Communication:
                              <span> {application.communication} </span>
              </div>
              <div className='label'> English Level:
                              <span> {application.english} </span>
              </div>
              <div className='label'> Available to Start:
                              <span> {application.date} </span>
              </div>
              <div className='label'> Technical Skills and Courses:
                              <div className='input'> {application.technical} </div>
              </div>
              <div className='label'> Short Personal Presentation:
                              <div className='input'> {application.personal} </div>
              </div>
              <div className='label'> Study from Home:
                              <span> {application.studyFromHome} </span>
              </div>
            </div>
          )
        })}
      </div >
    )
  }
}

export default Applications