import React from 'react'

class Form extends React.Component {
    constructor(props) {
        super(props)
        if (props.application) {
            this.state = props.application
        } else {
            this.state = {
                name: '',
                age: 0,
                phone: '',
                email: '',
                technical: '',
                personal: '',
            }
        }
    }
    onChange = (event) => {
        this.setState({ [event.target.name]: event.target.value })
    }
    onSubmit = (event) => {
        event.preventDefault()
        this.props.onSubmit(this.state)
    }
    render() {
        return (
            <form className='form' onSubmit={this.onSubmit}>
                <label> Name: <input type='text' name='name' value={this.state.name} onChange={this.onChange} /> </label>
                <label> Age: <input type='number' name='age' value={this.state.age} onChange={this.onChange} /> </label>
                <label> Phone: <input type='text' name='phone' value={this.state.phone} onChange={this.onChange} /> </label>
                <label> Email: <input type='text' name='email' value={this.state.email} onChange={this.onChange} /> </label>
                <label> Preferred Way of Communication:
                    <label>
                        <input type='radio' name='communication' value='phone' onChange={this.onChange} />
                        Phone
                    </label>
                    <label>
                        <input type='radio' name='communication' value='email' onChange={this.onChange} />
                        Email
                    </label>
                </label>
                <label> English Level:
                    <select name='english' onChange={this.onChange}>
                        <option value='beginner'> Beginner </option>
                        <option value='advanced'> Advanced </option>
                        <option value='fluent'> Fluent </option>
                    </select>
                </label>
                <label> Available to Start:
                    <input type='date' name='start' value={this.state.start} min='2020-02-25' onChange={this.onChange} />
                </label>
                <label>
                    <input type='checkbox' name='studyFromHome' onChange={this.onChange} />
                    Study from Home
                </label>
                <label> Technical Skills and Courses
                    <textarea name='technical' value={this.state.technical} onChange={this.onChange} />
                </label>
                <label> Short Peronal Presentation
                     <textarea name='personal' value={this.state.personal} onChange={this.onChange} />
                </label>

                <button type='submit'> Submit </button>
            </form>
        )
    }
}

export default Form