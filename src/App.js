import React from 'react'
import Modal from 'react-modal'
import Form from './components/Form'
import Applications from './components/Applications'
import Logo from './assets/settings-outline.svg'

Modal.setAppElement('#root')

const customStyles = {
  content: {
    width: '40%',
    borderRadius: '15px',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
}

class App extends React.Component {
  state = {
    modalIsOpen: false,
    applications: window.localStorage.getItem('applications')
      ? JSON.parse(window.localStorage.getItem('applications')).filter(Boolean)
      : [],
    editIndex: -1
  }
  handleModalClose = () => {
    this.setState({ modalIsOpen: false, editIndex: -1 })
  }
  handleModalOpen = () => {
    this.setState({ modalIsOpen: true })
  }
  onSubmit = (values) => {
    const editIndex = this.state.editIndex
    if (editIndex === -1) {
      this.setState({ applications: this.state.applications.concat([values]) }, () => {
        window.localStorage.setItem('applications', JSON.stringify(this.state.applications))
      })
    } else {
      const applications = [...this.state.applications]
      applications[editIndex] = values
      this.setState({ applications }, () => {
        window.localStorage.setItem('applications', JSON.stringify(this.state.applications))
      })
    }
    this.setState({ modalIsOpen: false, editIndex: -1 })
  }
  deleteApplication = (index) => {
    const applications = [...this.state.applications]
    delete applications[index]
    this.setState({ applications }, () => {
      window.localStorage.setItem('applications', JSON.stringify(this.state.applications))
    })
  }
  editApplication = (index) => {
    this.setState({ editIndex: index, modalIsOpen: true })
  }
  render() {
    return (
      <div className='wrapper'>
        <div className='header'>
          <div className='logo-container'>
            <img src={Logo} alt='Cog Logo' />
            <h2>Application Manager</h2>
          </div>
          <button onClick={this.handleModalOpen}> Create New </button>
        </div>
        <div className='body'>
          <Modal
            style={customStyles}
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.handleModalClose}
            contentLabel="Applicaiton Modal"
          >
            <Form onSubmit={this.onSubmit} application={this.state.applications[this.state.editIndex]} />
          </Modal>
          <Applications applications={this.state.applications} deleteApplication={this.deleteApplication} editApplication={this.editApplication} />
        </div>
      </div>
    )
  }
}

export default App
